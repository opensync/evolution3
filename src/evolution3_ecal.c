/*
 * evolution3_sync - A plugin for the opensync framework
 * Copyright (C) 2004-2005  Armin Bauer <armin.bauer@opensync.org>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 */
 
#include <string.h>
#include <glib.h>
#include <time.h>

#include <opensync/opensync.h>
#include <opensync/opensync-data.h>
#include <opensync/opensync-format.h>
#include <opensync/opensync-helper.h>
#include <opensync/opensync-plugin.h>

#include "evolution3_capabilities.h"
#include "evolution3_ecal.h"

// Generates a "hash" by grabbing the existing hash
// of the given uid and the content (data)
// TODO : prefer a revision field as contact entries 
//   lastdate_modified field exist ; but how use it !
//   from icomponent object, I don't know how to retriev the field
static char *evo3_ecal_generate_hash(const char *uid, char *data)
{
	char *hash = g_strdup_printf("%s-%s", uid, data);
	return hash;
}

ECalClient *evo3_ecal_open_cal(const char *path, ECalClientSourceType source_type, OSyncError **error)
{
	ECalClient *calendar = NULL;
	GError *gerror = NULL;
	ESource *source = NULL;

	osync_trace(TRACE_ENTRY, "%s(%s)", __func__, path);

#if EDS_CHECK_VERSION(3,6,0)
	ESourceRegistry *registry = NULL;
	GList *sources = NULL;
	const gchar *extension_type = NULL;

	registry = e_source_registry_new_sync(NULL, &gerror);
	if (!registry) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Error connecting to ESourceRegistry: %s", gerror ? gerror->message : "None");
		goto error;
	}
#else
	ESourceList *sources = NULL;
#endif

	if (!path) {
		osync_error_set(error, OSYNC_ERROR_MISCONFIGURATION, "No path set");
		goto error;
	}

#if EDS_CHECK_VERSION(3,6,0)
	switch (source_type) {
		case E_CAL_CLIENT_SOURCE_TYPE_EVENTS:
			extension_type = E_SOURCE_EXTENSION_CALENDAR;
			break;
		case E_CAL_CLIENT_SOURCE_TYPE_TASKS:
			extension_type = E_SOURCE_EXTENSION_TASK_LIST;
			break;
		case E_CAL_CLIENT_SOURCE_TYPE_MEMOS:
			extension_type = E_SOURCE_EXTENSION_MEMO_LIST;
			break;
		default:
			osync_error_set(error, OSYNC_ERROR_NOT_SUPPORTED, "Invalid Calendar Client Source Type: %d", source_type);
			goto error;
	}
#endif

	if (strcmp(path, "default")) {
#if EDS_CHECK_VERSION(3,6,0)
		sources = e_source_registry_list_sources(registry, extension_type);

		if (!(source = evo3_find_source(sources, path))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Error finding source \"%s\"", path);
			goto error;
		}
#else
		if (!e_cal_client_get_sources(&sources,source_type, &gerror)) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Unable to get sources for calendar: %s", gerror ? gerror->message : "None");
			goto error;
		}

		if (!(source = evo3_find_source(sources, path))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Error finding source \"%s\"", path);
			goto error;
		}
#endif
		if (!(calendar = e_cal_client_new(source, source_type, &gerror))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to create new calendar: %s", gerror ? gerror->message : "None");
			goto error;
		}
	} 
	else {
		osync_trace(TRACE_INTERNAL, "Opening default calendar\n");
#if EDS_CHECK_VERSION(3,6,0)
		source = e_source_registry_ref_default_for_extension_name(registry, extension_type);

		if (!(calendar = e_cal_client_new(source, source_type, &gerror))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to open default calendar for type %s: %s", extension_type, gerror ? gerror->message : "None");
			g_object_unref(source);
			goto error;
		}
		g_object_unref(source);
#else
		if (!(calendar = e_cal_client_new_default(source_type, &gerror))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to open default calendar: %s", gerror ? gerror->message : "None");
			goto error_free_cal;
		}
#endif
	}

	if(!e_client_open_sync(E_CLIENT(calendar), FALSE, NULL, &gerror)) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to open calendar: %s", gerror ? gerror->message : "None");
		goto error_free_cal;
	}

#if EDS_CHECK_VERSION(3,6,0)
	g_list_free_full (sources, g_object_unref);
	g_object_unref(registry);
#endif

	osync_trace(TRACE_EXIT, "%s", __func__);
	return calendar;
	
 error_free_cal:
	g_object_unref(calendar);
 error:
#if EDS_CHECK_VERSION(3,6,0)
	if (sources) g_list_free_full (sources, g_object_unref);
	if (registry) g_object_unref(registry);
#endif
	if (gerror)
		g_clear_error(&gerror);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return NULL;
}

static void evo3_ecal_connect(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, void *userdata)
{
	OSyncError *error = NULL;
       
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %p)", __func__, sink, info, ctx, userdata);
 	OSyncEvoCalendar * evo_cal = (OSyncEvoCalendar *)userdata;

	if (!(evo_cal->calendar = evo3_ecal_open_cal(evo_cal->uri, evo_cal->source_type, &error))) {
		goto error;
	}

	OSyncSinkStateDB *state_db = osync_objtype_sink_get_state_db(sink);
	osync_bool state_match;
	if (!state_db) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Anchor missing for objtype \"%s\"", osync_objtype_sink_get_name(sink));
		goto error_free_cal;
	}
	if (!osync_sink_state_equal(state_db, evo_cal->uri_key, evo_cal->uri, &state_match, &error)) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Anchor comparison failed for objtype \"%s\"", osync_objtype_sink_get_name(sink));
		goto error_free_cal;
	}
	if (!state_match) {
		osync_trace(TRACE_INTERNAL, "ECal slow sync, due to anchor mismatch for objtype \"%s\"", osync_objtype_sink_get_name(sink));
		osync_context_report_slowsync(ctx);
	}

	osync_context_report_success(ctx);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

error_free_cal:
	g_object_unref(evo_cal->calendar);
	evo_cal->calendar = NULL;
error:
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
}

static void evo3_ecal_disconnect(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %p)", __func__, sink, info, ctx, userdata);

	OSyncEvoCalendar * evo_cal = (OSyncEvoCalendar *)userdata;

	if (evo_cal->calendar) {
		g_object_unref(evo_cal->calendar);
		evo_cal->calendar = NULL;
	}

	osync_context_report_success(ctx);

	osync_trace(TRACE_EXIT, "%s", __func__);
}

static void evo3_ecal_sync_done(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p)", __func__, userdata, info, ctx);

	OSyncEvoCalendar * evo_cal = (OSyncEvoCalendar *)userdata;
	OSyncError *error = NULL;

	OSyncSinkStateDB *state_db = osync_objtype_sink_get_state_db(sink);
	if (!state_db) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "State database missing for objtype \"%s\"", osync_objtype_sink_get_name(sink));
		goto error;
	}

	if (!osync_sink_state_set(state_db, evo_cal->uri_key, evo_cal->uri, &error))
		goto error;

	osync_context_report_success(ctx);
        
	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

 error:
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
}


static void evo3_ecal_get_changes(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, osync_bool slow_sync, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %s, %p)", __func__, sink, info, ctx, slow_sync ? "TRUE" : "FALSE", userdata);
	OSyncEvoCalendar * evo_cal = (OSyncEvoCalendar *)userdata;
	OSyncError *error = NULL;
	OSyncChange *change = NULL;

	GSList *changes = NULL;
	GSList *l = NULL;
	char *data = NULL;
	const char *uid = NULL;
	int datasize = 0;
	GError *gerror = NULL;

	OSyncHashTable *hashtable = osync_objtype_sink_get_hashtable(sink);

	if (slow_sync) {
		osync_trace(TRACE_INTERNAL, "slow_sync for %s", evo_cal->objtype);
		if (!osync_hashtable_slowsync(hashtable, &error)) {
			osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed to get hashtable slowsync: %s", gerror ? gerror->message : "None");
			goto error;
		}
	}
	else {
		osync_trace(TRACE_INTERNAL, "No slow_sync for %s", evo_cal->objtype);
	}

	if (!e_cal_client_get_object_list_as_comps_sync(evo_cal->calendar, "#t", &changes, NULL, &gerror)) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed to get %s changes: %s",  evo_cal->objtype, gerror ? gerror->message : "None");
		goto error;
	}

	osync_trace(TRACE_INTERNAL, "Found %i %s", g_slist_length(changes), evo_cal->objtype);

	for (l = changes; l; l = l->next) {
		ECalComponent *comp = E_CAL_COMPONENT(l->data);

		e_cal_component_get_uid(comp, &uid);

		icalcomponent *icalcomp = e_cal_component_get_icalcomponent(comp);
		if (!icalcomp) {
			osync_error_set(&error, OSYNC_ERROR_GENERIC, "No ical component for uid %s", uid);
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			continue;
		}

		data = e_cal_client_get_component_as_string(evo_cal->calendar, icalcomp);
		if (!data) {
			osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed to get ical component for uid %s", uid);
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			continue;
		}

		datasize = strlen(data) + 1;

		osync_trace(TRACE_INTERNAL, "Component %s uid=%s - data=%p", evo_cal->objtype, uid, data);

		// create change to pass to hashtable
		change = osync_change_new(&error);
		if (!change) {
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			continue;
		}

		// setup change
		osync_change_set_uid(change, uid);

		char *hash = evo3_ecal_generate_hash(uid, data);
		osync_change_set_hash(change, hash);
		g_free(hash);

		// let hashtable determine what's going to happen
		OSyncChangeType changetype = osync_hashtable_get_changetype(hashtable, change);
		osync_change_set_changetype(change, changetype);

		// let hashtable know we've processed this change
		osync_hashtable_update_change(hashtable, change);

		// Decision time: if nothing has changed, skip
		if (changetype == OSYNC_CHANGE_TYPE_UNMODIFIED) {
			osync_change_unref(change);
			g_free(data);
			continue;
		}

		OSyncData *odata = osync_data_new(data, datasize, evo_cal->format, &error);
		if (!odata) {
			osync_change_unref(change);
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			g_free(data);
			continue;
		}

		osync_change_set_data(change, odata);
		osync_data_unref(odata);
		
		osync_context_report_change(ctx, change);

		osync_change_unref(change);
	}

	osync_trace(TRACE_INTERNAL, "%s proceed with success", evo_cal->objtype);
	e_client_util_free_object_slist(changes);

	OSyncList *u, *uids = osync_hashtable_get_deleted(hashtable);
	for (u = uids; u; u = u->next) {
		change = osync_change_new(&error);
		if( !change ) {
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			continue;
		}

		char *uid = u->data;
		osync_change_set_uid(change, uid);
		osync_change_set_changetype(change, OSYNC_CHANGE_TYPE_DELETED);

		osync_trace(TRACE_INTERNAL, "Component %s deleted uid=%s", evo_cal->objtype, uid);

		OSyncData *odata = osync_data_new(NULL, 0, evo_cal->format, &error);
		if (!odata) {
			osync_change_unref(change);
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			continue;
		}

		osync_data_set_objtype(odata, osync_objtype_sink_get_name(sink));
		osync_change_set_data(change, odata);
		osync_data_unref(odata);

		osync_context_report_change(ctx, change);

		osync_hashtable_update_change(hashtable, change);

		osync_change_unref(change);
	}
	osync_list_free(uids);
	
	osync_context_report_success(ctx);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

error:
	if (gerror)
		g_clear_error(&gerror);
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
}

static void evo3_ecal_read(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, OSyncChange *change, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %p, %p)", __func__, sink, info, ctx, change, userdata);
	OSyncEvoCalendar * evo_cal = (OSyncEvoCalendar *)userdata;
	OSyncError *error = NULL;

	char *data = NULL;
	const char *uid = NULL;
	int datasize = 0;
	GError *gerror = NULL;
	icalcomponent *icalcomp = NULL;

	uid = osync_change_get_uid(change);

	if (!e_cal_client_get_object_sync(evo_cal->calendar, uid, NULL, &icalcomp, NULL, &gerror)) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed to get %s uid %s: %s", evo_cal->objtype, uid, gerror ? gerror->message : "None");
		goto error;
	}

	osync_trace(TRACE_INTERNAL, "Found %s %s", evo_cal->objtype, uid);

	data = e_cal_client_get_component_as_string(evo_cal->calendar, icalcomp);
	if (!data) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed to get ical component for uid %s", uid);
		icalcomponent_free(icalcomp);
		goto error;
	}

	datasize = strlen(data) + 1;

	osync_trace(TRACE_INTERNAL, "Component %s uid=%s - data=%p", evo_cal->objtype, uid, data);

	char *hash = evo3_ecal_generate_hash(uid, data);
	osync_change_set_hash(change, hash);
	g_free(hash);

	OSyncData *odata = osync_data_new(data, datasize, evo_cal->format, &error);
	if (!odata) {
		g_free(data);
		icalcomponent_free(icalcomp);
		goto error;
	}

	osync_data_set_objtype(odata, osync_objtype_sink_get_name(sink));
	osync_change_set_data(change, odata);
	osync_data_unref(odata);
	
	icalcomponent_free(icalcomp);

	osync_context_report_success(ctx);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

error:
	if (gerror)
		g_clear_error(&gerror);
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
}

static void evo3_ecal_commit(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, OSyncChange *change, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %p, %p)", __func__, sink, info, ctx, change, userdata);

	OSyncEvoCalendar * evo_cal = (OSyncEvoCalendar *)userdata;
	OSyncHashTable *hashtable = osync_objtype_sink_get_hashtable(sink);

	const char *uid = osync_change_get_uid(change);
	char *hash = NULL;
	icalcomponent *iroot = NULL;
	icalcomponent *icomp = NULL;
	char *returnuid = NULL;
	GError *gerror = NULL;
	OSyncError *error = NULL;
	OSyncData *odata = NULL;
	char *data = NULL;
	char *plain = NULL;

	switch (osync_change_get_changetype(change)) {
		case OSYNC_CHANGE_TYPE_DELETED:
			osync_trace(TRACE_INTERNAL, "Deleting a component %s...", evo_cal->objtype);
			if (!e_cal_client_remove_object_sync(evo_cal->calendar, uid, 0, CALOBJ_MOD_ALL, NULL, &gerror)) {
				osync_error_set(&error, OSYNC_ERROR_GENERIC, "Unable to delete %s: %s", evo_cal->objtype, gerror ? gerror->message : "None");
				goto error;
			}
			osync_trace(TRACE_INTERNAL, "Component deleted uid=%s", uid);
			break;

		case OSYNC_CHANGE_TYPE_ADDED:
			osync_trace(TRACE_INTERNAL, "Adding a component %s...", evo_cal->objtype);
			odata = osync_change_get_data(change);
			osync_data_get_data(odata, &plain, NULL);
			iroot = icalcomponent_new_from_string(plain);
			if (!iroot) {
				osync_context_report_error(ctx, OSYNC_ERROR_GENERIC, "Unable to convert %s", evo_cal->objtype);
				goto error;
			}
			// note: icalcomponent_get_first_component() does not
			// allocate here, so no need to free *this* icomp
			icomp = icalcomponent_get_first_component (iroot, evo_cal->ical_component);
			if (!icomp) {
				osync_error_set(&error, OSYNC_ERROR_GENERIC, "Unable to get %s", evo_cal->objtype);
				icalcomponent_free(iroot);
				goto error;
			}
			if (e_cal_client_create_object_sync(evo_cal->calendar, icomp, &returnuid, NULL, &gerror)) {
				osync_change_set_uid(change, returnuid);
			}
			else {
				if (g_error_matches(gerror, E_CALENDAR_ERROR, E_CALENDAR_STATUS_OBJECT_ID_ALREADY_EXISTS)) {
					osync_error_set(&error, OSYNC_ERROR_EXISTS, "Unable to create %s: %s", evo_cal->objtype, gerror ? gerror->message : "None");
				} else {
					osync_error_set(&error, OSYNC_ERROR_GENERIC, "Unable to create %s: %s", evo_cal->objtype, gerror ? gerror->message : "None");
				}
				icalcomponent_free(iroot);
				goto error;
			}
			// freeing iroot nullifies our icomp, and
			// e_cal_client_get_object_sync() will allocate a
			// new one, if successful
			icalcomponent_free(iroot);
			if (e_cal_client_get_object_sync(evo_cal->calendar, returnuid, NULL, &icomp, NULL, &gerror)) {
				data = e_cal_client_get_component_as_string(evo_cal->calendar, icomp);
				hash = evo3_ecal_generate_hash(returnuid, data);
				osync_change_set_hash(change, hash);
				osync_trace(TRACE_INTERNAL, "%s added uid=%s - data=%p", evo_cal->objtype, returnuid, data);
				g_free(data);
				icalcomponent_free(icomp);
				g_free(hash);
			} else {
				/* should we do something here */
				g_clear_error(&gerror);
			}
			g_free(returnuid);
			break;
		case OSYNC_CHANGE_TYPE_MODIFIED:
			osync_trace(TRACE_INTERNAL, "Modifying a component %s...", evo_cal->objtype);
			odata = osync_change_get_data(change);
			osync_data_get_data(odata, &plain, NULL);

			iroot = icalcomponent_new_from_string(plain);
			if (!iroot) {
				osync_error_set(&error, OSYNC_ERROR_GENERIC, "Unable to convert %s", evo_cal->objtype);
				goto error;
			}

			// note: icalcomponent_get_first_component() does not
			// allocate here, so no need to free *this* icomp
			icomp = icalcomponent_get_first_component (iroot, evo_cal->ical_component);
			if (!icomp) {
				osync_error_set(&error, OSYNC_ERROR_GENERIC, "Unable to get %s", evo_cal->objtype);
				icalcomponent_free(iroot);
				goto error;
			}
			
			icalcomponent_set_uid (icomp, uid);
			if (e_cal_client_modify_object_sync(evo_cal->calendar, icomp, CALOBJ_MOD_ALL, NULL, &gerror)) {
			}
			else {
				/* try to add */
				osync_trace(TRACE_INTERNAL, "unable to mod %s: %s", evo_cal->objtype, gerror ? gerror->message : "None");

				g_clear_error(&gerror);
				if (e_cal_client_create_object_sync(evo_cal->calendar, icomp, &returnuid, NULL, &gerror)) {
					osync_change_set_uid(change, returnuid);
					uid = osync_change_get_uid(change);
					g_free(returnuid);
				}
				else {

					if (g_error_matches(gerror, E_CALENDAR_ERROR, E_CALENDAR_STATUS_OBJECT_ID_ALREADY_EXISTS)) {
						osync_error_set(&error, OSYNC_ERROR_EXISTS, "Unable to create %s: %s", evo_cal->objtype, gerror ? gerror->message : "None");
					} else {
						osync_error_set(&error, OSYNC_ERROR_GENERIC, "Unable to create %s: %s", evo_cal->objtype, gerror ? gerror->message : "None");
					}
					icalcomponent_free(iroot);
					goto error;
				}
			}
			icalcomponent_free(iroot);

			// TODO : we should better get the icomponent object from UID and RID 
			// But how get the RID ? 
			if (e_cal_client_get_object_sync(evo_cal->calendar, uid, NULL, &icomp, NULL, &gerror)) {
				data = e_cal_client_get_component_as_string(evo_cal->calendar, icomp);
				hash = evo3_ecal_generate_hash(uid, data);
				osync_change_set_hash(change, hash);
				osync_trace(TRACE_INTERNAL, "%s modified uid=%s - data=%p", evo_cal->objtype, uid, data);
				g_free(data);
				icalcomponent_free(icomp);
				g_free(hash);
			} else {
				/* should we do something here */
				g_clear_error(&gerror);
			}
			break;
		default:
			printf("Error\n");
	}

	osync_hashtable_update_change(hashtable, change);

	osync_context_report_success(ctx);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

error:
	if (gerror)
		g_clear_error(&gerror);
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
}

osync_bool evo3_ecal_discover(OSyncEvoCalendar *evo_cal, OSyncCapabilities *caps, OSyncError **error)
{
	ECalClient *cal = NULL;
	GError *gerror = NULL;
	gboolean writable;
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p)", __func__, evo_cal, caps, error);

	if (evo_cal->sink) {
		if (!(cal = evo3_ecal_open_cal(evo_cal->uri, evo_cal->source_type, error))) {
			goto error;
		}
		writable = !e_client_is_readonly(E_CLIENT(cal));
		osync_objtype_sink_set_write(evo_cal->sink, writable);
		osync_trace(TRACE_INTERNAL, "Set sink write status to %s", writable ? "TRUE" : "FALSE");

		g_object_unref(cal);
	}
	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;

 error:
	if (gerror)
		g_clear_error(&gerror);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
        return FALSE;
}

osync_bool evo3_ecal_initialize(OSyncEvoEnv *env, OSyncPluginInfo *info, const char *objtype, const char *required_format, OSyncError **error)
{
	char *uri_key;

	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p)", __func__, env, info, error);

	osync_assert(env);
	osync_assert(info);
	osync_assert(objtype);
	osync_assert(required_format);

	OSyncObjTypeSink *sink = osync_plugin_info_find_objtype(info, objtype);
	if (!sink)
		goto exit;
	osync_bool sinkEnabled = osync_objtype_sink_is_enabled(sink);
	osync_trace(TRACE_INTERNAL, "%s: enabled => %d", __func__, sinkEnabled);
	if (!sinkEnabled)
		goto exit;

	osync_objtype_sink_set_connect_func(sink, evo3_ecal_connect);
	osync_objtype_sink_set_disconnect_func(sink, evo3_ecal_disconnect);
	osync_objtype_sink_set_get_changes_func(sink, evo3_ecal_get_changes);
	osync_objtype_sink_set_read_func(sink, evo3_ecal_read);
	osync_objtype_sink_set_commit_func(sink, evo3_ecal_commit);
	osync_objtype_sink_set_sync_done_func(sink, evo3_ecal_sync_done);

	osync_objtype_sink_enable_state_db(sink, TRUE);

	OSyncEvoCalendar *cal = osync_try_malloc0(sizeof(OSyncEvoCalendar), error);
	if (!cal) {
		goto error;
	}
	cal->objtype = objtype;
	cal->change_id = env->change_id;

	OSyncPluginConfig *config = osync_plugin_info_get_config(info);
	OSyncPluginResource *resource = osync_plugin_config_find_active_resource(config, objtype);

	const int size = strlen(STR_URI_KEY) + strlen(objtype) + 1;

	uri_key = malloc(size * sizeof(char));

	snprintf(uri_key, size, "%s%s", STR_URI_KEY, objtype);

	cal->uri_key = uri_key;
	cal->uri = osync_plugin_resource_get_url(resource);
	if(!cal->uri) {
		osync_error_set(error,OSYNC_ERROR_MISCONFIGURATION, "%s url not set", objtype);
		goto error;
	}
	OSyncList *objformatsinks = osync_plugin_resource_get_objformat_sinks(resource);
	osync_bool hasObjFormat = FALSE;
	OSyncList *r;
	for(r = objformatsinks;r;r = r->next) {
		OSyncObjFormatSink *objformatsink = r->data;
		if(!strcmp(required_format, osync_objformat_sink_get_objformat(objformatsink))) { hasObjFormat = TRUE; break;}
	}
	osync_list_free(objformatsinks);
	if (!hasObjFormat) {
		osync_error_set(error, OSYNC_ERROR_MISCONFIGURATION, "Format %s not set.", required_format);
		goto error;
	}

	OSyncFormatEnv *formatenv = osync_plugin_info_get_format_env(info);
	cal->format = osync_format_env_find_objformat(formatenv, required_format);
	assert(cal->format);
	osync_objformat_ref(cal->format);
	
	if (strcmp(cal->objtype, "event") == 0) {
		cal->source_type = E_CAL_CLIENT_SOURCE_TYPE_EVENTS;
		cal->ical_component = ICAL_VEVENT_COMPONENT;
	}
	else if (strcmp(cal->objtype, "todo") == 0) {
		cal->source_type = E_CAL_CLIENT_SOURCE_TYPE_TASKS;
		cal->ical_component = ICAL_VTODO_COMPONENT;
	}
   	else if (strcmp(cal->objtype, "note") == 0) {
		cal->source_type = E_CAL_CLIENT_SOURCE_TYPE_MEMOS;
		cal->ical_component = ICAL_VJOURNAL_COMPONENT;
	}
   	else {
		osync_error_set(error, OSYNC_ERROR_NOT_SUPPORTED, "Unhandled objtype %s", cal->objtype);
		goto error;
	}

	cal->sink = osync_objtype_sink_ref(sink);

	osync_objtype_sink_set_userdata(cal->sink, cal);
	osync_objtype_sink_enable_hashtable(cal->sink, TRUE);

	env->calendars = g_list_append(env->calendars, cal);

 exit:
	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;

 error:
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}
