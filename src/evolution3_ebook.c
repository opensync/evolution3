/*
 * evolution3_sync - A plugin for the opensync framework
 * Copyright (C) 2004-2005  Armin Bauer <armin.bauer@opensync.org>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 */

#include <string.h>

#include <opensync/opensync.h>
#include <opensync/opensync-data.h>
#include <opensync/opensync-format.h>
#include <opensync/opensync-helper.h>
#include <opensync/opensync-plugin.h>

#include "evolution3_capabilities.h"

#include "evolution3_ebook.h"


// Generates a "hash" by grabbing the existing hash
// of the given uid and the revision
static char *evo3_ebook_generate_hash(const char *uid, const char *revision)
{
	char *hash = g_strdup_printf("%s-%s", uid, revision);
	return hash;
}

EBookClient *evo3_ebook_open_book(const char *path, OSyncError **error) 
{
	EBookClient *addressbook = NULL;
	GError *gerror = NULL;
	ESource *source = NULL;

	osync_trace(TRACE_ENTRY, "%s(%s)", __func__, path);

#if EDS_CHECK_VERSION(3,6,0)
	ESourceRegistry *registry = NULL;
	GList *sources = NULL;

	registry = e_source_registry_new_sync(NULL, &gerror);
	if (!registry) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Error connecting to ESourceRegistry: %s", gerror ? gerror->message : "None");
		goto error;
	}
#else
	ESourceList *sources = NULL;
#endif
	osync_trace(TRACE_ENTRY, "%s(%s, %p)", __func__, path, error);

	if (!path) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "no addressbook path set");
	  	goto error;
	}

	if (strcmp(path, "default")) {
#if EDS_CHECK_VERSION(3,6,0)
		sources = e_source_registry_list_sources(registry, E_SOURCE_EXTENSION_ADDRESS_BOOK);

		if (!(source = evo3_find_source(sources, path))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Error finding source \"%s\"", path);
			goto error;
		}
#else
		if (!e_book_client_get_sources(&sources, &gerror)) {
	  		osync_error_set(error, OSYNC_ERROR_GENERIC, "Error getting addressbooks: %s", gerror ? gerror->message : "None");
	  		goto error;
		}
		
		if (!(source = evo3_find_source(sources, path))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Error finding source \"%s\"", path);
	  		goto error;
		}
#endif
		if (!(addressbook = e_book_client_new(source, &gerror))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to alloc new addressbook: %s", gerror ? gerror->message : "None");
	  		goto error;
		}
	} else {
		osync_trace(TRACE_INTERNAL, "Opening default addressbook\n");
#if EDS_CHECK_VERSION(3,6,0)
		source = e_source_registry_ref_default_address_book(registry);

		if (!(addressbook = e_book_client_new(source, &gerror))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to alloc new default addressbook: %s", gerror ? gerror->message : "None");
			g_object_unref(source);
	  		goto error;
		}
		g_object_unref(source);
#else
		if (!(addressbook = e_book_client_new_default(&gerror))) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to alloc new default addressbook: %s", gerror ? gerror->message : "None");
	  		goto error;
		}
#endif
	}

	if (!e_client_open_sync(E_CLIENT(addressbook), TRUE, NULL, &gerror)) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to open addressbook: %s", gerror ? gerror->message : "None");
	  	goto error_free_book;
	}

#if EDS_CHECK_VERSION(3,6,0)
	g_list_free_full (sources, g_object_unref);
	g_object_unref(registry);
#endif

	osync_trace(TRACE_EXIT, "%s", __func__);
	return addressbook;


 error_free_book:
	g_object_unref(addressbook);
 error:
#if EDS_CHECK_VERSION(3,6,0)
	if (sources) g_list_free_full (sources, g_object_unref);
	if (registry) g_object_unref(registry);
#endif
	if (gerror)
		g_clear_error(&gerror);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return NULL;

}

osync_bool evo3_ebook_discover(OSyncEvoEnv *env, OSyncCapabilities *caps, OSyncError **error) 
{
	EBookClient *book = NULL;
	GError *gerror = NULL;
	gboolean success;
	gboolean writable;
	gchar *fields[2] = { NULL, NULL };

	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p)", __func__, env, caps, error);
	osync_assert(env);
	osync_assert(caps);

	if (env->contact_sink) {
		if (!(book = evo3_ebook_open_book(env->addressbook_path, error))) {
			goto error;
		}
		writable = !e_client_is_readonly(E_CLIENT(book));
		osync_objtype_sink_set_write(env->contact_sink, writable);
		osync_trace(TRACE_INTERNAL, "Set sink write status to %s", writable ? "TRUE" : "FALSE");

		success = e_client_get_backend_property_sync (E_CLIENT(book), BOOK_BACKEND_PROPERTY_SUPPORTED_FIELDS, &fields[0], NULL, &gerror);

		g_object_unref(book);
		if (!success) {
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Failed to get supported fields: %s", gerror ? gerror->message : "None");
			goto error;
		}
		
		success = evo3_capabilities_translate_ebook(caps, fields, error);
		if (!success) {
			goto error;
		}
	}
	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;
 error:
	if (gerror)
		g_clear_error(&gerror);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;

}

static void evo3_ebook_connect(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, void *userdata)
{
	OSyncError *error = NULL;
	
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %p)", __func__, sink, info, ctx, userdata);
	OSyncEvoEnv *env = (OSyncEvoEnv *)userdata;
	osync_bool state_match;

	if (!(env->addressbook = evo3_ebook_open_book(env->addressbook_path, &error))) {
		goto error;
	}
	
	OSyncSinkStateDB *state_db = osync_objtype_sink_get_state_db(sink);
	if (!state_db) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "State database missing for objtype \"%s\"", osync_objtype_sink_get_name(sink));
		goto error_free_book;
	}
	if (!osync_sink_state_equal(state_db, "path", env->addressbook_path, &state_match, &error)) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Anchor comparison failed for objtype \"%s\"", osync_objtype_sink_get_name(sink));
		goto error_free_book;
	}
	if (!state_match) {
		osync_trace(TRACE_INTERNAL, "EBook slow sync, due to anchor mismatch");
		osync_context_report_slowsync(ctx);
	}

	
	osync_context_report_success(ctx);
	
	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

 error_free_book:
	g_object_unref(env->addressbook);
	env->addressbook = NULL;
 error:
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
}

static void evo3_ebook_disconnect(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p)", __func__, userdata, info, ctx);
	OSyncEvoEnv *env = (OSyncEvoEnv *)userdata;
	
	if (env->addressbook) {
		g_object_unref(env->addressbook);
		env->addressbook = NULL;
	}
	
	osync_context_report_success(ctx);
	
	osync_trace(TRACE_EXIT, "%s", __func__);
}

static void evo3_ebook_sync_done(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %p)", __func__, sink, info, ctx, userdata);

	OSyncEvoEnv *env = (OSyncEvoEnv *)userdata;
	OSyncError *error = NULL;

	OSyncSinkStateDB *state_db = osync_objtype_sink_get_state_db(sink);
	if (!state_db) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "State database missing for objtype \"%s\"", osync_objtype_sink_get_name(sink));
		goto error;
	}

	if (!osync_sink_state_set(state_db, "path", env->addressbook_path, &error))
		goto error;
	
	osync_context_report_success(ctx);
	
	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

 error:
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
	
}

static void evo3_ebook_get_changes(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, osync_bool slow_sync, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %s, %p)", __func__, sink, info, ctx, slow_sync ? "TRUE" : "FALSE", userdata);
	OSyncEvoEnv *env = (OSyncEvoEnv *)userdata;
	OSyncError *error = NULL;
	OSyncChange *change = NULL;
	
	GSList *changes = NULL;
	EVCard vcard;
	GSList *l = NULL;
	char *data = NULL;
	char *uid = NULL;
	char *revision = NULL;
	int datasize = 0;
	GError *gerror = NULL;

	OSyncHashTable *hashtable = osync_objtype_sink_get_hashtable(sink);

	if (slow_sync) {
		osync_trace(TRACE_INTERNAL, "slow_sync for contact");
		if (!osync_hashtable_slowsync(hashtable, &error)) {
			osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed to get hashtable slowsync: %s", gerror ? gerror->message : "None");
			goto error;
		}
	}
	else {
		osync_trace(TRACE_INTERNAL, "No slow_sync for contact");
	}

	EBookQuery *query = e_book_query_any_field_contains("");
	gchar *sexp = e_book_query_to_string(query);
	if (!e_book_client_get_contacts_sync(env->addressbook, sexp, &changes, NULL, &gerror)) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed to get changes from addressbook: %s", gerror ? gerror->message : "None");
		goto error;
	}

	osync_trace(TRACE_INTERNAL, "Found %i contacts", g_slist_length(changes));
	for (l = changes; l; l = l->next) {
		EContact *contact = E_CONTACT(l->data);
		vcard = contact->parent;
		data = e_vcard_to_string(&vcard, EVC_FORMAT_VCARD_30);
		uid = g_strdup(e_contact_get_const(contact, E_CONTACT_UID));
		revision = g_strdup(e_contact_get_const(contact, E_CONTACT_REV));
		datasize = strlen(data) + 1;

		osync_trace(TRACE_INTERNAL, "Contact uid=%s - revision=%s", uid, revision);

		// create change to pass to hashtable
		change = osync_change_new(&error);
		if( !change ) {
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			g_free(revision);
			g_free(uid);
			g_free(data);
			continue;
		}

		// setup change
		osync_change_set_uid(change, uid);

		char *hash = evo3_ebook_generate_hash(uid, revision);
		osync_change_set_hash(change, hash);
		g_free(hash);

		// let hashtable determine what's going to happen
		OSyncChangeType changetype = osync_hashtable_get_changetype(hashtable, change);
		osync_change_set_changetype(change, changetype);

		// let hashtable know we've processed this change
		osync_hashtable_update_change(hashtable, change);

		// Decision time: if nothing has changed, skip
		if (changetype == OSYNC_CHANGE_TYPE_UNMODIFIED) {
			osync_change_unref(change);
			g_free(revision);
			g_free(uid);
			g_free(data);
			continue;
		}

		OSyncData *odata = osync_data_new(data, datasize, env->contact_format, &error);
		if (!odata) {
			osync_change_unref(change);
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			g_free(revision);
			g_free(uid);
			g_free(data);
			continue;
		}

		osync_change_set_data(change, odata);
		osync_data_unref(odata);

		osync_context_report_change(ctx, change);

		osync_change_unref(change);

		g_free(revision);
		g_free(uid);
	}
	osync_trace(TRACE_INTERNAL, "contacts proceed with success");
	e_client_util_free_object_slist(changes);
	e_book_query_unref(query);
	g_free(sexp);

	OSyncList *u, *uids = osync_hashtable_get_deleted(hashtable);
	for (u = uids; u; u = u->next) {
		change = osync_change_new(&error);
		if( !change ) {
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			continue;
		}

		char *uid = u->data;
		osync_change_set_uid(change, uid);
		osync_change_set_changetype(change, OSYNC_CHANGE_TYPE_DELETED);

		osync_trace(TRACE_INTERNAL, "Contact deleted uid=%s", uid);

		OSyncData *odata = osync_data_new(NULL, 0, env->contact_format, &error);
		if (!odata) {
			osync_change_unref(change);
			osync_context_report_osyncwarning(ctx, error);
			osync_error_unref(&error);
			continue;
		}

		osync_data_set_objtype(odata, osync_objtype_sink_get_name(sink));
		osync_change_set_data(change, odata);
		osync_data_unref(odata);

		osync_context_report_change(ctx, change);

		osync_hashtable_update_change(hashtable, change);

		osync_change_unref(change);
	}
	osync_list_free(uids);
	
	osync_context_report_success(ctx);
	
	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

error:
	if (gerror)
		g_clear_error(&gerror);
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
}

static void evo3_ebook_read(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, OSyncChange *change, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %p, %p)", __func__, sink, info, ctx, change, userdata);
	OSyncEvoEnv *env = (OSyncEvoEnv *)userdata;
	OSyncError *error = NULL;
	
	EVCard vcard;
	char *data = NULL;
	const char *uid = NULL;
	char *revision = NULL;
	int datasize = 0;
	GError *gerror = NULL;
	EContact *contact = NULL;

	uid = osync_change_get_uid(change);

	if (!e_book_client_get_contact_sync(env->addressbook, uid, &contact, NULL, &gerror)) {
		osync_error_set(&error, OSYNC_ERROR_GENERIC, "Failed to get uid %s from addressbook: %s", uid, gerror ? gerror->message : "None");
		goto error;
	}

	osync_trace(TRACE_INTERNAL, "Found contact %s", uid);

	vcard = contact->parent;
	data = e_vcard_to_string(&vcard, EVC_FORMAT_VCARD_30);
	revision = g_strdup(e_contact_get_const(contact, E_CONTACT_REV));
	datasize = strlen(data) + 1;

	osync_trace(TRACE_INTERNAL, "Contact uid=%s - revision=%s", uid, revision);

	char *hash = evo3_ebook_generate_hash(uid, revision);
	osync_change_set_hash(change, hash);
	g_free(hash);
	g_free(revision);

	OSyncData *odata = osync_data_new(data, datasize, env->contact_format, &error);
	if (!odata) {
		g_free(data);
		g_object_unref(contact);
		goto error;
	}

	osync_data_set_objtype(odata, osync_objtype_sink_get_name(sink));
	osync_change_set_data(change, odata);
	osync_data_unref(odata);

	g_object_unref(contact);

	osync_context_report_success(ctx);

	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

error:
	if (gerror)
		g_clear_error(&gerror);
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
}

static void evo3_ebook_commit(OSyncObjTypeSink *sink, OSyncPluginInfo *info, OSyncContext *ctx, OSyncChange *change, void *userdata)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p, %p, %p)", __func__, sink, info, ctx, change, userdata);

	OSyncEvoEnv *env = (OSyncEvoEnv *)userdata;
	OSyncHashTable *hashtable = osync_objtype_sink_get_hashtable(sink);
	
	const char *uid = osync_change_get_uid(change);
	char *hash = NULL;
	char *revision = NULL;
	EContact *contact = NULL;
	GError *gerror = NULL;
	OSyncError *error = NULL;
	OSyncData *odata = NULL;
	char *plain = NULL;

	switch (osync_change_get_changetype(change)) {
		case OSYNC_CHANGE_TYPE_DELETED:
			osync_trace(TRACE_INTERNAL, "Deleting a contact...");
			if (!e_book_client_remove_contact_by_uid_sync(env->addressbook, uid, NULL, &gerror)) {
				osync_error_set(&error, OSYNC_ERROR_GENERIC, "Unable to delete contact: %s", gerror ? gerror->message : "None");
				goto error;
			}
			osync_trace(TRACE_INTERNAL, "Contact deleted uid=%s", uid);
			break;

		case OSYNC_CHANGE_TYPE_ADDED:
			osync_trace(TRACE_INTERNAL, "Adding a contact...");
			odata = osync_change_get_data(change);
			osync_data_get_data(odata, &plain, NULL);
			contact = e_contact_new_from_vcard(plain);
			e_contact_set(contact, E_CONTACT_UID, NULL);
			if (e_book_client_add_contact_sync(env->addressbook, contact, (gchar **) &uid, NULL, &gerror)) {
				osync_change_set_uid(change, uid);
			}
			else {
				g_object_unref(contact);
				if (g_error_matches(gerror, E_BOOK_ERROR, E_BOOK_ERROR_CONTACT_ID_ALREADY_EXISTS)) {
					osync_error_set(&error, OSYNC_ERROR_EXISTS, "Unable to add contact: %s", gerror ? gerror->message : "None");
				} else {
					osync_error_set(&error, OSYNC_ERROR_GENERIC, "Unable to add contact: %s", gerror ? gerror->message : "None");
				}
				goto error;
			}
			g_object_unref(contact);
			if (e_book_client_get_contact_sync(env->addressbook, uid, &contact, NULL, &gerror)) {
				revision = g_strdup(e_contact_get_const(contact, E_CONTACT_REV));
				hash = evo3_ebook_generate_hash(uid, revision);
				osync_change_set_hash(change, hash);
				osync_trace(TRACE_INTERNAL, "Contact added uid=%s - revision=%s", uid, revision);
				g_object_unref(contact);
				g_free(hash);
				g_free(revision);
			} else {
				/* should we do something here */
				g_clear_error(&gerror);
			}
			break;

		case OSYNC_CHANGE_TYPE_MODIFIED:
			osync_trace(TRACE_INTERNAL, "Modifing a contact...");
			odata = osync_change_get_data(change);
			osync_data_get_data(odata, &plain, NULL);
			
			contact = e_contact_new_from_vcard(plain);
			e_contact_set(contact, E_CONTACT_UID, g_strdup(uid));
			
			osync_trace(TRACE_INTERNAL, "ABout to modify vcard:\n%s", e_vcard_to_string(&(contact->parent), EVC_FORMAT_VCARD_30));
			
			if (e_book_client_modify_contact_sync(env->addressbook, contact, NULL, &gerror)) {
				uid = e_contact_get_const (contact, E_CONTACT_UID);
				if (uid)
					osync_change_set_uid(change, uid);
			}
			else {
				/* try to add */
				osync_trace(TRACE_INTERNAL, "unable to mod contact: %s", gerror ? gerror->message : "None");
				
				g_clear_error(&gerror);
				if (e_book_client_add_contact_sync(env->addressbook, contact, NULL, NULL, &gerror)) {
					uid = e_contact_get_const(contact, E_CONTACT_UID);
					osync_change_set_uid(change, uid);
				}
				else {
					g_object_unref(contact);
					if (gerror->code == E_BOOK_ERROR_CONTACT_ID_ALREADY_EXISTS) {
						osync_error_set(&error, OSYNC_ERROR_EXISTS, "Unable to add contact: %s", gerror ? gerror->message : "None");
					} else {
						osync_error_set(&error, OSYNC_ERROR_GENERIC, "Unable to add contact: %s", gerror ? gerror->message : "None");
					}
					goto error;
				}
			}
			g_object_unref(contact);
			if (e_book_client_get_contact_sync(env->addressbook, uid, &contact, NULL, &gerror)) {
				revision = g_strdup(e_contact_get_const(contact, E_CONTACT_REV));
				hash = evo3_ebook_generate_hash(uid, revision);
				osync_change_set_hash(change, hash);
				osync_trace(TRACE_INTERNAL, "Contact modified uid=%s - revision=%s", uid, revision);
				g_object_unref(contact);
				g_free(hash);
				g_free(revision);
			} else {
				/* should we do something here */
				g_clear_error(&gerror);
			}
			break;
		default:
			printf("Error\n");
	}
	
	osync_hashtable_update_change(hashtable, change);

	osync_context_report_success(ctx);
	
	osync_trace(TRACE_EXIT, "%s", __func__);
	return;

error:
	if (gerror)
		g_clear_error(&gerror);
	osync_context_report_osyncerror(ctx, error);
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(&error));
	osync_error_unref(&error);
}


osync_bool evo3_ebook_initialize(OSyncEvoEnv *env, OSyncPluginInfo *info, OSyncError **error)
{
	osync_trace(TRACE_ENTRY, "%s(%p, %p, %p)", __func__, env, info, error);
	OSyncObjTypeSink *sink = osync_plugin_info_find_objtype(info, "contact");
	if (!sink) {
		osync_trace(TRACE_INTERNAL, "No sink for objtype contact, ebook not initialized");
		goto exit;
	}
	osync_bool sinkEnabled = osync_objtype_sink_is_enabled(sink);
        osync_trace(TRACE_INTERNAL, "%s: enabled => %d", __func__, sinkEnabled);
	if (!sinkEnabled) {
		osync_trace(TRACE_INTERNAL, "Sink for objtype contact not enabled, ebook not initialized");
		goto exit;
	}
	
	osync_objtype_sink_set_connect_func(sink, evo3_ebook_connect);
	osync_objtype_sink_set_disconnect_func(sink, evo3_ebook_disconnect);
	osync_objtype_sink_set_get_changes_func(sink, evo3_ebook_get_changes);
	osync_objtype_sink_set_commit_func(sink, evo3_ebook_commit);
	osync_objtype_sink_set_read_func(sink, evo3_ebook_read);
	osync_objtype_sink_set_sync_done_func(sink, evo3_ebook_sync_done);

	osync_objtype_sink_enable_state_db(sink, TRUE);

	OSyncPluginConfig *config = osync_plugin_info_get_config(info);
	OSyncPluginResource *resource = osync_plugin_config_find_active_resource(config, "contact");
	env->addressbook_path = osync_plugin_resource_get_url(resource);
	if(!env->addressbook_path) {
		osync_error_set(error,OSYNC_ERROR_MISCONFIGURATION, "Addressbook url not set");
		goto error;
	}
	OSyncList *objformatsinks = osync_plugin_resource_get_objformat_sinks(resource);
	osync_bool hasObjFormat = FALSE;
	OSyncList *r;
	for(r = objformatsinks;r;r = r->next) {
		OSyncObjFormatSink *objformatsink = r->data;
		if(!strcmp("vcard30", osync_objformat_sink_get_objformat(objformatsink))) { hasObjFormat = TRUE; break;}
	}
	osync_list_free(objformatsinks);
        if (!hasObjFormat) {
		osync_error_set(error, OSYNC_ERROR_MISCONFIGURATION, "Format vcard30 not set.");
		goto error;
	}

	OSyncFormatEnv *formatenv = osync_plugin_info_get_format_env(info);
	env->contact_format = osync_format_env_find_objformat(formatenv, "vcard30");
	assert(env->contact_format);
	osync_objformat_ref(env->contact_format);

	env->contact_sink = osync_objtype_sink_ref(sink);

	osync_objtype_sink_set_userdata(sink, env);
	osync_objtype_sink_enable_hashtable(sink, TRUE);

 exit:
	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;

 error:
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}

