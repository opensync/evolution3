/*
 * evo3-sync - A plugin for the opensync framework
 * Copyright (C) 2008 Ian Martin <ianmartin@cantab.net>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 */

#include <opensync/opensync.h>
#include <opensync/opensync-capabilities.h>

#include <glib.h>

#include "evolution3_capabilities.h"

osync_bool evo3_translate_capabilities(OSyncCapabilities *caps, gchar **fields, const char *objtype, OSyncError **error)
{
	int i = 0;
	GSList *field = NULL;

	osync_trace(TRACE_ENTRY, "%s(%p, %p, %s, %p)", __func__, caps, fields, objtype, error);
	osync_assert(caps);
	osync_assert(fields);
	osync_assert(objtype);

	OSyncCapabilitiesObjType *capsobjtype = osync_capabilities_add_new_objtype(caps, objtype, error);

	for(i=0; fields[i]; i++) {
		osync_trace(TRACE_INTERNAL, "capabilities fields list %s", (const char *) fields[i]);

		field = e_client_util_parse_comma_strings(fields[i]);

		for(; field; field = g_slist_next(field)) {
			OSyncCapability *cap = osync_capabilities_add_new_capability(capsobjtype, error);
			if (!cap)
				goto error;
		
			osync_trace(TRACE_INTERNAL, "capabilities field %s", (const char *) field->data);

			osync_capability_set_name(cap, (const char *) field->data);
		}

		while (field) {
			g_free(field->data);
			field = g_slist_remove(field, field->data);
		}
	}

	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;

 error:
	while (field) {
		g_free(field->data);
		field = g_slist_remove(field, field->data);
	}
	osync_trace(TRACE_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}

osync_bool evo3_capabilities_translate_ebook(OSyncCapabilities *caps, gchar **fields, OSyncError **error) {
	return evo3_translate_capabilities(caps, fields, "contact", error);
}
