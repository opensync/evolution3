// see note in ../src/evolution2_sync.h
#define HANDLE_LIBICAL_MEMORY 1
#include <glib.h>

#include <libedataserver/eds-version.h>
#if EDS_CHECK_VERSION(3,6,0)
#include <libecal/libecal.h>
#include <libebook/libebook.h>
#include <libedataserver/libedataserver.h>
#else
#include <libecal/e-cal-client.h>
#include <libecal/e-cal.h>
#include <libebook/e-book-client.h>
#include <libedataserver/e-data-server-util.h>
#endif


#if EDS_CHECK_VERSION(3,6,0)
void print_sources(GList *sources)
{
	ESource *source = NULL;

	GList *s = NULL;
	for (s = sources; s; s = s->next) {
		source = E_SOURCE (s->data);
		printf("  %s: %s\n", e_source_get_display_name(source), e_source_get_uid(source));
	}
}
#else
void print_sources(ESourceList *sources)
{
	ESource *source = NULL;

	GSList *g = NULL;
	for (g = e_source_list_peek_groups (sources); g; g = g->next) {
		ESourceGroup *group = E_SOURCE_GROUP (g->data);
		printf("Group: %s\n", e_source_group_peek_name(group));

		GSList *s = NULL;
		for (s = e_source_group_peek_sources (group); s; s = s->next) {
			source = E_SOURCE (s->data);
			printf("  %s: %s\n", e_source_peek_name(source), e_source_get_uri(source));
		}
	}
}
#endif

void print_calendars(ECalClientSourceType source_type) {
#if EDS_CHECK_VERSION(3,6,0)
	ESourceRegistry *registry = NULL;
	GError *gerror = NULL;
	GList *sources = NULL;
	const gchar *extension_type = NULL;

	switch (source_type) {
		case E_CAL_CLIENT_SOURCE_TYPE_EVENTS:
			extension_type = E_SOURCE_EXTENSION_CALENDAR;
			break;
		case E_CAL_CLIENT_SOURCE_TYPE_TASKS:
			extension_type = E_SOURCE_EXTENSION_TASK_LIST;
			break;
		case E_CAL_CLIENT_SOURCE_TYPE_MEMOS:
			extension_type = E_SOURCE_EXTENSION_MEMO_LIST;
			break;
		default:
			extension_type = "";
	}

	registry = e_source_registry_new_sync(NULL, &gerror);
	sources = e_source_registry_list_sources(registry, extension_type);
	print_sources(sources);
	g_list_free_full (sources, g_object_unref);
	g_object_unref(registry);
#else
	ESourceList *sources = NULL;
	if (e_cal_get_sources(&sources, source_type, NULL)) {
		print_sources(sources);
	}
#endif
}

void print_memos()
{
	print_calendars(E_CAL_CLIENT_SOURCE_TYPE_MEMOS);
}

void print_tasks()
{
	print_calendars(E_CAL_CLIENT_SOURCE_TYPE_TASKS);
}

void print_events()
{
	print_calendars(E_CAL_CLIENT_SOURCE_TYPE_EVENTS);
}

int main () {
#if EDS_CHECK_VERSION(3,6,0)
	GList *sources = NULL;
	ESourceRegistry *registry = NULL;
	GError *gerror = NULL;
#else
	ESourceList *sources = NULL;
#endif

#if !GLIB_CHECK_VERSION (2, 36, 0)
	g_type_init();
#endif

	printf("Addressbooks:\n");
#if EDS_CHECK_VERSION(3,6,0)
	registry = e_source_registry_new_sync(NULL, &gerror);
	sources = e_source_registry_list_sources(registry, E_SOURCE_EXTENSION_ADDRESS_BOOK);
	print_sources(sources);
#else
	if (e_book_client_get_sources(&sources, NULL)) {
		print_sources(sources);
	}
#endif
	printf("\n");
	printf("Events:\n");
	print_events();
	printf("\n");

	printf("Tasks:\n");
	print_tasks();
	printf("\n");
	
	printf("Memos:\n");
	print_memos();

#if EDS_CHECK_VERSION(3,6,0)
	g_list_free_full (sources, g_object_unref);
	g_object_unref(registry);
#endif

	return 0;
}
