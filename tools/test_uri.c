// see note in ../src/evolution2_sync.h
#define HANDLE_LIBICAL_MEMORY 1
#include <string.h>
#include <glib.h>

#include <libedataserver/eds-version.h>
#if EDS_CHECK_VERSION(3,6,0)
#include <libecal/libecal.h>
#include <libebook/libebook.h>
#include <libedataserver/libedataserver.h>
#else
#include <libecal/e-cal.h>
#include <libebook/e-book.h>
#include <libedataserver/e-data-server-util.h>
#endif

void open_calendar(const char *uri, ECalSourceType source_type)
{
#if EDS_CHECK_VERSION(3,6,0)
	ESourceRegistry *registry = NULL;
	ESource *source = NULL;
	ECalClient *cal = NULL;
	GError *gerror = NULL;
	const gchar *extension_type = NULL;

	switch (source_type) {
		case E_CAL_CLIENT_SOURCE_TYPE_EVENTS:
			extension_type = E_SOURCE_EXTENSION_CALENDAR;
			break;
		case E_CAL_CLIENT_SOURCE_TYPE_TASKS:
			extension_type = E_SOURCE_EXTENSION_TASK_LIST;
			break;
		case E_CAL_CLIENT_SOURCE_TYPE_MEMOS:
			extension_type = E_SOURCE_EXTENSION_MEMO_LIST;
			break;
		default:
			printf("Invalid Calendar Client Source Type: %d", source_type);
			return;
	}

	registry = e_source_registry_new_sync(NULL, &gerror);
	source = e_source_registry_ref_source(registry, uri);
	cal = e_cal_client_new(source, source_type, &gerror);

	if (!cal) {
		printf("Failed to allocate calendar\n");
		g_object_unref(source);
		g_object_unref(registry);
		return;
	}

	if (!e_client_open_sync(E_CLIENT(cal), TRUE, NULL, &gerror)) {
		printf("Failed to open calendar:\n%s\n", gerror->message);
		g_clear_error(&gerror);
		g_object_unref(cal);
		g_object_unref(source);
		g_object_unref(registry);
		return;
	}
	printf("Successfully opened %s\n", uri);
	g_object_unref(cal);

	g_object_unref(source);
	g_object_unref(registry);
#else
	ECal *cal = NULL;
	GError *gerror = NULL;
	cal = e_cal_new_from_uri(uri, source_type);

	if (!cal) {
		printf("Failed to allocate calendar\n");
		return;
	}
	if (!e_cal_open(cal, TRUE, &gerror)) {
		printf("Failed to open calendar:\n%s\n", gerror->message);
		g_clear_error(&gerror);
		return;
	}
	printf("Successfully opened %s\n", uri);
	g_object_unref(cal);

#endif
}

void open_book(const char *uri)
{
#if EDS_CHECK_VERSION(3,6,0)
	ESourceRegistry *registry = NULL;
	ESource *source = NULL;
	EBookClient *book = NULL;
	GError *gerror = NULL;

	registry = e_source_registry_new_sync(NULL, &gerror);
	source = e_source_registry_ref_source(registry, uri);
	book = e_book_client_new(source, &gerror);

	if (book) {
		if (e_client_open_sync(E_CLIENT(book), TRUE, NULL, &gerror)) {
			printf("Successfully opened %s\n", uri);
		}
		g_object_unref(book);
	}
	if (gerror) {
		printf("Failed to open addressbook:\n%s\n", gerror->message);
		g_clear_error(&gerror);
	}

	g_object_unref(source);
	g_object_unref(registry);
#else
	EBook *book = NULL;
	GError *gerror = NULL;
	book = e_book_new_from_uri(uri, &gerror);

	if (book) {
		if (e_book_open(book, TRUE, &gerror)) {
			printf("Successfully opened %s\n", uri);
		}
		g_object_unref(book);
	}
	if (gerror) {
		printf("Failed to open addressbook:\n%s\n", gerror->message);
		g_clear_error(&gerror);
	}
#endif
}

static void usage(char *progname, int exitcode)
{
	fprintf (stderr, "Usage: %s <objtype> <uri>\n\n", progname);
	fprintf (stderr, "objtype may be one of:\n");
	fprintf (stderr, "--contact\n");
	fprintf (stderr, "--event\n");
	fprintf (stderr, "--note\n");
	fprintf (stderr, "--todo\n");
	fprintf (stderr, "\nWARNING: This program allows you to attempt to open ECals with the wrong source type.  Doing so may break everything\n");
	exit(exitcode);
}

int main(int argc, char *argv[])
{

#if !GLIB_CHECK_VERSION (2, 36, 0)
	g_type_init();
#endif
	if (argc < 2)
		usage(argv[0], 1);
	if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
		usage(argv[0], 0);
		    
	if (argc <3)
		usage(argv[0], 1);
	if (!strcmp(argv[1], "--contact")) {
		open_book(argv[2]);
	} else if (!strcmp(argv[1], "--event")) {
		open_calendar(argv[2], E_CAL_SOURCE_TYPE_EVENT);
	} else if (!strcmp(argv[1], "--todo")) {
		open_calendar(argv[2], E_CAL_SOURCE_TYPE_TODO);
	} else if (!strcmp(argv[1], "--note")) {
		open_calendar(argv[2], E_CAL_SOURCE_TYPE_JOURNAL);
	} else {
		usage(argv[0], 1);
	}
	return 0;
}
